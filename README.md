# ddstat

A tool that send to Datadog the data while execute dstat.

[![Gem Version](https://badge.fury.io/rb/ddstat.png)](http://badge.fury.io/rb/ddstat)

## Installation

Add this line to your application's Gemfile:

    gem 'ddstat'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install ddstat

## Usage

```sh
$ export DD_API_KEY=...
$ ddstat -tclmsdrn 5 # The outputted data is transmitted to Datadog.
----system---- ----total-cpu-usage---- ---load-avg--- ------memory-usage----- ----swap--- -dsk/total- --io/total- -net/total-
  date/time   |usr sys idl wai hiq siq| 1m   5m  15m | used  buff  cach  free| used  free| read  writ| read  writ| recv  send
16-02 07:51:06|  1   1  98   0   0   0|   0 0.06 0.22| 235M 44.7M 1156M 13.2G|   0     0 |9792B   69k|0.79  0.81 |   0     0
16-02 07:51:11|  0   0 100   0   0   0|   0 0.06 0.22| 236M 44.7M 1156M 13.2G|   0     0 |   0    27k|   0  3.20 |  48B  127B
16-02 07:51:16|  0   0 100   0   0   0|   0 0.06 0.22| 236M 44.7M 1156M 13.2G|   0     0 |   0     0 |   0     0 |  10B   46B
...
```

![datadog-metrics.png](https://bitbucket.org/winebarrel/ddstat/downloads/datadog-metrics.png)

## Contributing

1. Fork it ( http://github.com/<my-github-username>/ddstat/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
