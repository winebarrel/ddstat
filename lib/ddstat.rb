module Ddstat
  def self.run
    ::Ddstat::Ddstat.new.run
  end
end

require 'dogapi'
require 'pty'
require 'socket'
require 'time'
require 'thread'
require 'ddstat/version'
require 'ddstat/ddstat'
